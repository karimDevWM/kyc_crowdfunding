-- Active: 1672651624498@@127.0.0.1@3306@kyc_crowdfunding
CREATE EVENT IF NOT EXISTS total_invests_projects_event
ON SCHEDULE EVERY 1 MINUTE
DO
    CALL total_invest_projects();