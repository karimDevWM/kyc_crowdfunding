-- Active: 1672651624498@@127.0.0.1@3306@kyc_crowdfunding

CREATE TABLE funding(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    amount FLOAT COMMENT 'amount',
    payment_date datetime,
    status VARCHAR(255)
) COMMENT '';

CREATE TABLE Role(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    label VARCHAR (50)
    ) COMMENT '';

CREATE TABLE Comment(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    text VARCHAR (50),
    post_date datetime DEFAULT now()
) COMMENT '';

CREATE TABLE Users(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    firstName VARCHAR (50),
    lastName VARCHAR (50),
    email VARCHAR (50),
    password VARCHAR (50),
    address VARCHAR (50),
    role_id INT,
    FOREIGN KEY (role_id) REFERENCES Role (id)
) COMMENT '';

CREATE TABLE User_Comment(
    id INT NOT NULL,
    user_id INT NOT NULL, 
    comment_id INT NOT NULL,

    FOREIGN KEY (user_id) REFERENCES Users(id),
    FOREIGN KEY (comment_id) REFERENCES Comment(id)

) COMMENT '';

/* ALTER TABLE user_comment
ADD CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES Users(id),
ADD CONSTRAINT fk_comment FOREIGN KEY (comment_id) REFERENCES Comment(id); */


CREATE TABLE Project(  
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    title VARCHAR (50),
    description VARCHAR (50)
) COMMENT '';

/* jointures tables */
CREATE TABLE user_project_funding(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    user_id INT,
    project_id INT,
    funding_id INT,
    Foreign Key (user_id) REFERENCES users(id),
    Foreign Key (project_id) REFERENCES project(id),
    Foreign Key (funding_id) REFERENCES funding(id)
);

CREATE TABLE user_project_founding(
    id INT NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT 'Primary Key',
    user_id INT,
    project_id INT,
    status BOOLEAN DEFAULT NULL,
    Foreign Key (user_id) REFERENCES user (id),
    Foreign Key (project_id) REFERENCES project (id)
);


